import React, { useState, useEffect, useRef } from 'react';
import './App.css';
import AppBarComponent from './AppBarComponent';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ImageUpload from './ImageUpload';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import IconButton from '@material-ui/core/IconButton';
import CheckIcon from '@material-ui/icons/Check';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as tf from '@tensorflow/tfjs';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '5rem',
    textAlign: 'center'
  },
  action: {
    textAlign: 'center',
    marginTop: '2rem'
  },
  imagePreview: {
    height: '350px',
  },
}));

function App() {
  const classes = useStyles();

  const [model, setModel] = useState();
  const photoRef = useRef(null);

  const [predictions, setPredictions] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    tf.loadLayersModel(window.location.origin + '/model/model.json').then((model) => {
      setModel(model);
    })
  }, []);

  const [image, setImage] = useState({
    file: '',
    imagePreviewUrl: '',
  });

  const handleChange = image => {
    setIsLoading(true);

    setImage(image);

    setTimeout(() => {
      let offset = tf.scalar(255);

      let tensor = tf.browser.fromPixels(photoRef.current)
        .resizeNearestNeighbor([150, 150])
        .toFloat()
        .div(offset)
        .expandDims();

      model.predict(tensor).data().then(predictions => {
        const pred = [];
        predictions.forEach(e => pred.push(parseInt(e * 100)))
        setPredictions(pred);
        setIsLoading(false);
      })
    }, 2000)
  }

  const labels = [
    {
      name: 'AMRD',
      humanName: 'Age-related Macular Degeneration'
    },
    {
      name: 'CSR',
      humanName: 'Central serous retinopathy'
    },
    {
      name: 'DR',
      humanName: 'Diabetic Retinopathy'
    },
    {
      name: 'MH',
      humanName: 'Macular Hole'
    },
    {
      name: 'NORMAL',
      humanName: 'Normal'
    }
  ]

  const indexOfMaxPred = predictions.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);

  return (
    <div className="App">
      <AppBarComponent />

      <Container>
        <div className={classes.action}>
          <Typography variant="h4">Select a photo</Typography>
          <ImageUpload onChange={handleChange} onClear={() => setImage({
            file: '',
            imagePreviewUrl: '',
          })} />
        </div>

        {image.imagePreviewUrl && (
          <div>
            <div style={{
              marginLeft: '2rem',
              marginRight: '2rem',
              marginTop: '2rem'
            }}>
              <hr />
            </div>

            <Grid container spacing={5} className={classes.root}>
              <Grid item xs={6}>
                <Card className={classes.imagePreview}>
                  <CardHeader
                    title="Preview"
                  />
                  <CardContent className={classes.backgroundColor}>
                    <img src={image.imagePreviewUrl} height={150} width={150} ref={photoRef} />
                  </CardContent>
                </Card>
              </Grid>
              <Grid item xs={6}>
                <Card className={classes.imagePreview}>
                  <CardHeader
                    title="Diagnosis"
                  />
                  <CardContent className={classes.backgroundColor}>
                    {isLoading ? <LoadingComponent /> : 
                    <List component="nav" aria-label="secondary mailbox folders">
                      {predictions.map((e, i) => (
                        <ListItem button key={i} style={{ color: i === indexOfMaxPred ? 'green' : 'black' }}>
                          {i === indexOfMaxPred && <ListItemIcon>
                            <CheckIcon />
                          </ListItemIcon>}
                          <ListItemText primary={labels[i].humanName} />
                          <ListItemSecondaryAction>
                            <IconButton edge="end" aria-label="delete">
                              <Typography>{`${e}%`}</Typography>
                            </IconButton>
                          </ListItemSecondaryAction>
                        </ListItem>
                      ))}
                    </List>}
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </div>
        )}
      </Container>
    </div>
  );
}

const LoadingComponent = () => {
  return (
    <div style={{ marginTop: '4rem'}}>
      <CircularProgress />
    </div>
  )
}


export default App;
