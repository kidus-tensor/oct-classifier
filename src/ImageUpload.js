import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '500px'
    },
}));

const ImageUpload = props => {
    const classes = useStyles();

    const _handleClear = (e) => {
        e.preventDefault();

        props.onClear();
    }

    const _handleImageChange = (e) => {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            props.onChange({
                file: file,
                imagePreviewUrl: reader.result
            })
        }

        reader.readAsDataURL(file)
    }

    return (
        <form>
            <input type="file" onChange={_handleImageChange} />
            <button type="submit" onClick={_handleClear}>Clear</button>
        </form>
    );
}

export default ImageUpload;
